import React from "react";
import Context from "./ProductsContext";
import { getProducts } from "../../services/api";
import useFetch from "../../hooks/useFetch";
const ProductsProvider = ({ children }) => {
  const [shoppingCart, setShoppingCart] = React.useState([]);
  const [totalItems, setTotalItems] = React.useState(0);
  const [subTotal, setSubTotal] = React.useState(null);
  const { data, error, loading, request } = useFetch();
  const value = {
    data,
    error,
    loading,
    shoppingCart,
    setShoppingCart,
    totalItems,
    setTotalItems,
    subTotal,
    setSubTotal,
  };
  React.useEffect(() => {
    async function fetchData() {
      await request(getProducts());
    }
    fetchData();
  }, []);

  return <Context.Provider value={value}>{children}</Context.Provider>;
};

export default ProductsProvider;
