import { useContext } from "react";

import Context from "./ProductsContext";

const useProductsContext = () => useContext(Context);

export default useProductsContext;
