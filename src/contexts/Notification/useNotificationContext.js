import { useContext } from "react";

import Context from "./NotificationContext";

const useNotificationContext = () => useContext(Context);

export default useNotificationContext;
