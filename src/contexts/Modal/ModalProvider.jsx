import React from "react";
import Context from "./ModalContext";

const ModalProvider = ({ children }) => {
  const [isOpenModal, setIsOpenModal] = React.useState(false);
  const value = { isOpenModal, setIsOpenModal };

  return <Context.Provider value={value}>{children}</Context.Provider>;
};

export default ModalProvider;
