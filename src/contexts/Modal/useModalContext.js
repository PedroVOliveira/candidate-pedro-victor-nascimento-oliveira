import { useContext } from "react";
import Context from "./ModalContext";
const useModalContext = () => useContext(Context);

export default useModalContext;
