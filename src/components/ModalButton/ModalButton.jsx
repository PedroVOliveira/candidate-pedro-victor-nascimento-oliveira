import React from "react";
import styled from "styled-components";
import Icon from "../Icon/";
import { base } from "./styled";

const Wrapper = styled.div`
  ${base}
`;

const ModalButton = ({ children, onClickAdd, onClickRemove }) => {
  return (
    <Wrapper>
      <Icon icon="remove" color="#72bf44" size={22} onClick={onClickRemove} />
      <p>{children}</p>
      <Icon icon="add" color="#72bf44" size={22} onClick={onClickAdd} />
    </Wrapper>
  );
};

export default ModalButton;
