import React from "react";
import styled from "styled-components";
import { base } from "./styled";
import { useProductsContext } from "../../contexts/Products/";
import Line from "../Line";
const Footer = styled.footer`
  ${base}
`;

const ModalFooter = ({ freight }) => {
  const { subTotal } = useProductsContext();
  return (
    <Footer>
      <Line palette="dashed" />
      <div>
        <span>Frete:</span>
        <strong>{freight}</strong>
      </div>
      <div>
        <span>Subtotal:</span>
        <strong>R$ {subTotal}</strong>
      </div>
    </Footer>
  );
};

export default ModalFooter;
