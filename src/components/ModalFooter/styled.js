import { css } from "styled-components";

export const base = css`
  div {
    display: flex;
    justify-content: space-between;
    margin: 20px 0;
    span {
      font-size: 16px;
    }
    strong {
      font-size: 20px;
      font-weight: bold;
    }
  }
`;
