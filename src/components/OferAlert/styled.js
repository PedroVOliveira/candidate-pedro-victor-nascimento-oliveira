import { css } from "styled-components";
import { pixelToRem } from "../../settings";
export const base = css`
  display: flex;
  justify-content: space-between;
  align-items: center;
  background-color: ${(props) => props.theme.palette.redColor};
  color: ${(props) => props.theme.palette.yellowTextColor};
  position: relative;
  top: ${pixelToRem(-10)};
  height: ${pixelToRem(10)};
  font-size: ${pixelToRem(8)};
  padding: 1rem;
  box-sizing: border-box;
`;
