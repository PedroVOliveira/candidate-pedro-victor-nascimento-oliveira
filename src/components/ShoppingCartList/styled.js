import { css } from "styled-components";

export const base = css`
  display: flex;
  margin-bottom: 20px;
  h3 {
    color: ${(props) => props.theme.palette.primaryColor};
    font-size: 16px;
    font-weight: 400;
  }
  div {
    main {
      display: flex;
      flex-direction: column;
      height: max-contain;

      span {
        margin-bottom: 5px;
      }
    }
    div {
      display: flex;
      justify-content: space-between;
    }

    span {
      color: ${(props) => props.theme.palette.initialColor};
      opacity: 0.8;
      margin-top: 5px;
    }
    p {
      display: flex;
      flex-direction: column;
      text-align: right;
      span,
      small {
        font-size: 12px;
      }
      small {
        text-decoration-line: line-through;
        color: ${(props) => props.theme.palette.initialColor};
      }

      strong {
        display: block;
        font-size: 14px;
        font-weight: bold;
      }
    }
  }
`;
