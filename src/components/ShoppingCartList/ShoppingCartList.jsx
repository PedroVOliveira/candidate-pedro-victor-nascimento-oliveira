import React from "react";
import styled from "styled-components";
import { base } from "./styled";
import ModalImage from "../ModalImage/ModalImage";
import ModalButton from "../ModalButton";
import { useProductsContext } from "../../contexts/Products";

const ShoppingCartCard = styled.div`
  ${base}
`;

const ShoppingCartList = () => {
  const {
    shoppingCart,
    setShoppingCart,
    totalItems,
    setTotalItems,
    subTotal,
    setSubTotal,
  } = useProductsContext();

  function handleClickAdd(id) {
    const findItemInShoppingCart = shoppingCart.find(
      (product) => product.item.id === id
    );
    const shoppingCartExist = shoppingCart
      .map((product) => product.item.id)
      .includes(id);
    let priceConcated = `${findItemInShoppingCart?.item?.price?.to?.integers},${findItemInShoppingCart?.item?.price?.to?.decimals}`;
    let parsedPrice = parseFloat(
      priceConcated.replace(/\./g, "").replace(",", ".")
    );
    let totalValue = shoppingCart
      ?.map((product) => {
        return parseFloat(product?.price.replace(/\./g, "").replace(",", "."));
      })
      .reduce((a, b) => a + b, parsedPrice);
    setSubTotal(
      Intl.NumberFormat("pt-BR", {
        minimumFractionDigits: 2,
        maximumFractionDigits: 2,
      }).format(totalValue)
    );
    if (shoppingCartExist) {
      let newshoppingCart = shoppingCart.map((product) => {
        if (product.item.id === id) {
          product.quantity = product.quantity + 1;
          parsedPrice = parsedPrice * product.quantity;
          product.price = Intl.NumberFormat("pt-BR", {
            minimumFractionDigits: 2,
            maximumFractionDigits: 2,
          }).format(parsedPrice);
          setTotalItems(totalItems + 1);
        }
        return product;
      });
      setShoppingCart(newshoppingCart);
    } else {
      setTotalItems(totalItems + 1);
    }
  }
  function handleClickRemove(id) {
    const findItemInShoppingCart = shoppingCart.find(
      (product) => product.item.id === id
    );
    let priceConcated = `${findItemInShoppingCart?.item?.price?.to?.integers},${findItemInShoppingCart?.item?.price?.to?.decimals}`;
    let parsedPrice = parseFloat(
      priceConcated.replace(/\./g, "").replace(",", ".")
    );
    const shoppingCartExist = shoppingCart
      .map((product) => product.item.id)
      .includes(id);
    if (shoppingCartExist) {
      let newshoppingCart = shoppingCart.map((product) => {
        if (product.item.id === id) {
          if (product.quantity >= 1) {
            product.quantity = product.quantity - 1;
            parsedPrice = parsedPrice * product.quantity;
            product.price = Intl.NumberFormat("pt-BR", {
              minimumFractionDigits: 2,
              maximumFractionDigits: 2,
            }).format(parsedPrice);
            setTotalItems(totalItems - 1);
          }
        }
        return product;
      });
      setShoppingCart(newshoppingCart);
    } else {
      setTotalItems(totalItems - 1);
    }

    let totalValue = shoppingCart
      ?.map((product) => {
        return parseFloat(product?.price.replace(/\./g, "").replace(",", "."));
      })
      .reduce((a, b) => a + b);
    setSubTotal(
      Intl.NumberFormat("pt-BR", {
        minimumFractionDigits: 2,
        maximumFractionDigits: 2,
      }).format(totalValue)
    );
  }
  return (
    <>
      {shoppingCart?.map(({ item, quantity, price }) => (
        <ShoppingCartCard key={item.id}>
          <ModalImage src={item.picture} alt={item.name} />
          <div>
            <h3>{item.name}</h3>
            <div>
              <main>
                <span>Cód:{item.id}</span>
                <ModalButton
                  onClickAdd={() => handleClickAdd(item.id)}
                  onClickRemove={() => handleClickRemove(item.id)}
                >
                  {quantity}
                </ModalButton>
              </main>
              <p>
                {item.price.from !== null ? (
                  <small>
                    1 un. R${" "}
                    {`${item.price.from.integers},${item.price.from.decimals}`}
                  </small>
                ) : null}
                <span>
                  1 un. R$
                  {` ${item.price.to.integers}` + `,${item.price.to.decimals} `}
                </span>
                <strong>
                  {quantity} un. R$ {price}
                </strong>
              </p>
            </div>
          </div>
        </ShoppingCartCard>
      ))}
    </>
  );
};

export default ShoppingCartList;
