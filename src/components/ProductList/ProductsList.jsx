import React from "react";
import { useProductsContext } from "../../contexts/Products";
import { useNotificationContext } from "../../contexts/Notification";
import Card from "../Card";
import Button from "../Button";
import OferAlert from "../OferAlert";
import ProductPrice from "../ProductPrice";
import Loader from "../Loader";
import Exclusive from "../Exclusive";
const ProductsList = () => {
  const {
    data,
    error,
    loading,
    shoppingCart,
    setShoppingCart,
    totalItems,
    setTotalItems,
    subTotal,
    setSubTotal,
  } = useProductsContext();
  const { setIsOpen } = useNotificationContext();

  function handleClick(id) {
    setIsOpen(true);
    let qtd = 1;
    const findItemInData = data.find((product) => product.id === id);
    let priceConcated = `${findItemInData.price.to.integers},${findItemInData.price.to.decimals}`;
    let parsedPrice = parseFloat(
      priceConcated.replace(/\./g, "").replace(",", ".")
    );
    const shoppingCartExist = shoppingCart
      .map((product) => product.item.id)
      .includes(id);
    let totalValue = shoppingCart
      ?.map((product) => {
        return parseFloat(product?.price.replace(/\./g, "").replace(",", "."));
      })
      .reduce((a, b) => a + b, parsedPrice);
    setSubTotal(
      Intl.NumberFormat("pt-BR", {
        minimumFractionDigits: 2,
        maximumFractionDigits: 2,
      }).format(totalValue)
    );
    if (shoppingCartExist) {
      let newShoppingCart = shoppingCart.map((product) => {
        if (product.item.id === id) {
          product.quantity = product.quantity + 1;
          parsedPrice = parsedPrice * product.quantity;
          product.price = Intl.NumberFormat("pt-BR", {
            minimumFractionDigits: 2,
            maximumFractionDigits: 2,
          }).format(parsedPrice);
          setTotalItems(totalItems + 1);
        }
        return product;
      });
      setShoppingCart(newShoppingCart);
    } else {
      setShoppingCart([
        ...shoppingCart,
        { item: findItemInData, quantity: qtd, price: priceConcated },
      ]);
      setTotalItems(totalItems + 1);
    }
  }
  return (
    <>
      {loading && <Loader />}
      {!loading &&
        !error &&
        data &&
        data.map((product, index) => (
          <Card
            key={product.id}
            palette={
              product.price.from != null
                ? "borderNotGreen"
                : "borderBottomGreen"
            }
          >
            {product.tag != null ? (
              <Exclusive>{product.tag.label}</Exclusive>
            ) : null}
            <img src={product.picture} alt={product.name} />
            {product.offer != null ? (
              <OferAlert>-{product.offer.value}%</OferAlert>
            ) : null}
            <h3>{product.name}</h3>
            <Button palette="primary" onClick={() => handleClick(product.id)}>
              Adicionar ao Carrinho
            </Button>
            <ProductPrice
              palette={product.price.from != null ? "warning" : "defaultStyle"}
            >
              <strong>
                R$
                {` ${product.price.to.integers}` +
                  `,${product.price.to.decimals} `}
                <span>{product.unit}</span>
              </strong>
              {product.price.from != null ? (
                <small>
                  R${" "}
                  {`${product.price.from.integers},${product.price.from.decimals}`}
                </small>
              ) : null}

              <p>
                {product.installments != null
                  ? `${product.installments.amount}x de ${product.installments.value}`
                  : null}
              </p>
            </ProductPrice>
          </Card>
        ))}
      {!loading && error && <p>Não foi possivel fazer a requisição</p>}
    </>
  );
};

export default ProductsList;
