import React from "react";
import styled from "styled-components";
import { base, defaultStyle, warning } from "./styled";

const Wrapper = styled.div`
  ${base}
  ${(props) => props.palette === "warning" && warning};
  ${(props) => props.palette === "defaultStyle" && defaultStyle};
`;

const ProductPrice = ({ children, palette = "defaultStyle" }) => {
  return <Wrapper palette={palette}>{children}</Wrapper>;
};

export default ProductPrice;
