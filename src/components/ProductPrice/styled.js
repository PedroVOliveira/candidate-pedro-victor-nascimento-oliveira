import { css } from "styled-components";
import { pixelToRem } from "../../settings";
export const base = css`
  display: flex;
  flex-direction: column;
  align-items: end;
  justify-content: center;
  margin: 10px 5px;
  strong {
    font-size: ${pixelToRem(12)};

    span {
      font-size: ${pixelToRem(10)};
    }
  }

  small {
    /* margin: 10px 5px; */
    font-size: ${pixelToRem(8)};
    text-decoration-line: line-through;
    color: ${(props) => props.theme.palette.innitialColor};
    opacity: 0.6;
  }

  p {
    /* margin: 10px 5px; */
    margin: 0;
    padding: 0;
    font-size: ${pixelToRem(8)};
    color: ${(props) => props.theme.palette.innitialColor};
    font-weight: 700;
    opacity: 0.6;
  }
`;

export const warning = css`
  background-color: ${(props) => props.theme.palette.yellowColor};
  margin: 15px 0 0px 0px;
  padding: 10px 5px;
  strong {
    color: ${(props) => props.theme.palette.redColor};
  }
`;

export const defaultStyle = css`
  background-color: white;

  strong {
    color: black;
  }
`;
