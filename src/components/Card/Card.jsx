import React from "react";
import styled from "styled-components";
import { base, borderNotGreen, borderBottomGreen } from "./styled";

const Wrapper = styled.div`
  ${base}
  ${(props) => props.palette === "borderNotGreen" && borderNotGreen}
  ${(props) => props.palette === "borderBottomGreen" && borderBottomGreen}
`;

const Card = ({ children, palette = "borderBottomGreen" }) => {
  return <Wrapper palette={palette}>{children}</Wrapper>;
};

export default Card;
