import { css } from "styled-components";
import { pixelToRem } from "../../settings";
export const base = css`
  border-style: solid;
  border-color: #f0f0f0 #f0f0f0 green #f0f0f0;
  border-width: 2px 2px 5px 2px;
  margin: 5px 5px;
  position: relative;

  h3 {
    font-size: ${pixelToRem(8)};
    font-weight: normal;
    margin: 10px 5px;
  }

  @media (min-width: 700px) {
    margin: 10px 10px;
    display: flex;
    flex-direction: column;
    justify-content: center;
  }
`;

export const borderNotGreen = css`
  border-style: solid;
  border-color: #f0f0f0 #f0f0f0 #f0f0f0 #f0f0f0;
  border-width: 2px 2px 1px 2px;
`;

export const borderBottomGreen = css`
  border-style: solid;
  border-color: #f0f0f0 #f0f0f0 green #f0f0f0;
  border-width: 2px 2px 5px 2px;
`;
