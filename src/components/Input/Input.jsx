import React from "react";
import styled from "styled-components";

import { base, inputElBase, isValid } from "./styled";

const Wrapper = styled.div`
  ${base}
  ${isValid}
`;

const InputEl = styled.input`
  ${inputElBase}
`;

const Input = React.forwardRef(({ isValid, className, ...rest }, ref) => {
  return (
    <Wrapper className={className}>
      <InputEl {...rest} ref={ref} />
    </Wrapper>
  );
});

export default Input;
