import styled, { createGlobalStyle } from "styled-components";

import { ROOT_FONT_SIZE, mediaQueries } from "../../settings";

export const GlobalStyles = createGlobalStyle`
  @import url('https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@400;600;700&display=swap');

  * {
    margin:0;
    padding:0;
  }

  body {
    font-size: ${ROOT_FONT_SIZE};
    font-family: 'Source Sans Pro', sans-serif;
    padding: 0;
    margin: 0;
    scroll-behavior: smooth;
	};

`;

export const Container = styled.div`
  width: 95vw;
  margin: 0 auto;

  @media (min-width: 700px) {
    max-width: 1440px;
  }
`;

export const GridContainer = styled.div`
  display: grid;
  grid-template-columns: 50% 1fr;
  padding-top: 60px;

  img {
    width: 100%;
  }

  @media (min-width: 700px) {
    grid-template-columns: 0.2fr 0.2fr 0.2fr;
    margin: 0 auto;

    img {
      max-width: 100%;
    }
  }
`;
