import { css } from "styled-components";

export const base = css`
  border: 1px solid rgb(161, 155, 162);
  border-radius: 10px;
  height: 100px;
  margin-right: 10px;
  width: 100px;
`;
