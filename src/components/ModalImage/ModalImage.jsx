import React from "react";
import styled from "styled-components";
import { base } from "./styled";

const ImageModal = styled.img`
  ${base}
`;

const ModalImage = ({ src, alt }) => {
  return <ImageModal src={src} alt={alt} />;
};

export default ModalImage;
