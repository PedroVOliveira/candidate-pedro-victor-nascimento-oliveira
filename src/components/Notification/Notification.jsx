import React from "react";
import styled from "styled-components";
import { Container } from "../GlobalStyles/GlobalStyles";
import { base } from "./styled";
import { useNotificationContext } from "../../contexts/Notification/";
import Icon from "../Icon";
const Menssage = styled.div`
  ${base}
`;

const Notification = () => {
  const { isOpen, setIsOpen } = useNotificationContext();
  function handleClick() {
    setIsOpen(false);
  }
  if (isOpen) {
    return (
      <Menssage>
        <Container>
          <span>Item Adicionado no carrinho com sucesso</span>
          <Icon icon="clear" size={32} color="white" onClick={handleClick} />
        </Container>
      </Menssage>
    );
  }
  return null;
};

export default Notification;
