import { css } from "styled-components";
import { pixelToRem } from "../../settings";
export const base = css`
  background-color: green;
  color: white;
  width: 100%;
  height: 40px;
  position: fixed;
  z-index: 10;
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-top: 40px;
  font-size: ${pixelToRem(8)};

  div {
    display: flex;
    justify-content: space-between;
    align-items: center;
  }
`;
