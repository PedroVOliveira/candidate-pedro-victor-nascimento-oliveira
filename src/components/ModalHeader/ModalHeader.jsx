import React from "react";
import styled from "styled-components";
import { base } from "./styled";
import Icon from "../Icon";
import { useModalContext } from "../../contexts/Modal";
const HeaderModal = styled.header`
  ${base}
`;
const ModalHeader = () => {
  const { isOpenModal, setIsOpenModal } = useModalContext();
  function handleClick() {
    setIsOpenModal(!isOpenModal);
  }
  return (
    <HeaderModal>
      <Icon icon="clear" size={32} color="green" onClick={handleClick} />
      <h3>Produtos no Carrinho</h3>
    </HeaderModal>
  );
};

export default ModalHeader;
