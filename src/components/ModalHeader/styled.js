import { css } from "styled-components";

export const base = css`
  margin: 24px 0 30px;
  display: flex;
  align-items: center;

  h3 {
    font-size: 20px;
  }
`;
