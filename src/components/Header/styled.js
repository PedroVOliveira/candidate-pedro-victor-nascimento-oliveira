import { css } from "styled-components";
import { pixelToRem } from "../../settings";
export const base = css`
  position: fixed;
  z-index: 10;
  width: 100%;
  height: ${pixelToRem(30)};
  color: white;
  display: flex;
  justify-content: space-between;
  align-items: center;

  div {
    display: flex;
    justify-content: space-between;
    align-items: center;
  }
  h2 {
    font-size: ${pixelToRem(12)};
  }
  button {
    display: flex;
    background: none;
    border: 0;
    outline: none;
    cursor: pointer;
  }
  span {
    position: relative;
    left: -15px;
    background-color: ${(props) => props.theme.palette.redColor};
    color: white;
    border-radius: 100%;
    width: 20px;
    height: 20px;
    display: flex;
    justify-content: center;
    align-items: center;
  }
`;

export const primary = css`
  background-color: ${(props) => props.theme.palette.primaryColor};
`;
