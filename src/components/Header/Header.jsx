import React from "react";
import styled from "styled-components";
import { base, primary } from "./styled";
import { Container } from "../GlobalStyles/GlobalStyles";
import Icon from "../Icon";
import { useProductsContext } from "../../contexts/Products";
import { useModalContext } from "../../contexts/Modal";
const HeaderPage = styled.header`
  ${base}
  ${(props) => props.palette === "primary" && primary}
`;
const Header = ({ palette = "primary" }) => {
  const { totalItems } = useProductsContext();
  const { isOpenModal, setIsOpenModal } = useModalContext();
  return (
    <HeaderPage palette={palette}>
      <Container>
        <h2>Os melhores Produtos</h2>
        <button onClick={() => setIsOpenModal(!isOpenModal)}>
          <Icon icon="shopping_cart" color="white" size={32} />
          <span>{totalItems}</span>
        </button>
      </Container>
    </HeaderPage>
  );
};

export default Header;
