import React from "react";
import styled from "styled-components";
import { solid, dashed } from "./styled";
const LineDiv = styled.div`
  ${(props) => props.palette === "solid" && solid}
  ${(props) => props.palette === "dashed" && dashed}
`;
const Line = ({ palette = "solid" }) => {
  return <LineDiv palette={palette}></LineDiv>;
};

export default Line;
