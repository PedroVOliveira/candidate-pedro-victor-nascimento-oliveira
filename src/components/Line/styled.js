import { css } from "styled-components";

export const solid = css`
  margin: 10px 0px 16px;
  width: 100%;
  border-top: 1px solid rgb(238, 238, 240);
`;

export const dashed = css`
  margin: 10px 0px 16px;
  width: 100%;
  border-top: 2px dashed rgb(238, 238, 240);
`;
