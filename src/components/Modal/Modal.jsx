import React from "react";
import styled from "styled-components";
import Input from "../Input";
import ModalHeader from "../ModalHeader";
import ModalFooter from "../ModalFooter";
import Line from "../Line";
import ShoppingCartList from "../ShoppingCartList";
import { backdropBase } from "./styled";
import { useModalContext } from "../../contexts/Modal";
import useFetch from "../../hooks/useFetch";
import { getCEP } from "../../services/api";
const ModalBackdrop = styled.div`
  ${backdropBase};
`;
const ContainerModal = styled.div`
  padding: 0 15px;
`;
const Modal = () => {
  const { isOpenModal } = useModalContext();
  const [input, setInput] = React.useState(null);
  const [freight, setFreight] = React.useState(null);
  const { request } = useFetch();
  React.useEffect(() => {
    if (input && input.length === 8) {
      request(getCEP(input)).then(({ json }) => setFreight(json));
      if (freight) {
        let testing = parseFloat(freight);
        console.log(testing);
      }
    }
  }, [input]);
  console.log(freight);
  function handleChange({ target }) {
    setInput(target.value);
  }
  if (isOpenModal) {
    return (
      <ModalBackdrop>
        <ContainerModal>
          <ModalHeader />
          <Input
            type="number"
            placeholder="Calcular CEP"
            onChange={handleChange}
          />
          <Line palette="solid" />
          <ShoppingCartList />
          <ModalFooter freight={freight?.freight} />
        </ContainerModal>
      </ModalBackdrop>
    );
  }

  return null;
};

export default Modal;
