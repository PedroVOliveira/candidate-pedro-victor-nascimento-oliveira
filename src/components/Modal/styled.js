import { css } from "styled-components";

export const backdropBase = css`
  position: fixed;
  left: 0;
  top: 0;
  right: 0;
  bottom: 0;
  background-color: white;
  z-index: 12;

  @media (min-width: 700px) {
    max-width: 40%;
    left: auto;
    right: 0;
    margin-left: 10px;
  }
`;
