import React from "react";
import styled from "styled-components";
import { base } from "./styled";

const Wrapper = styled.div`
  ${base}
`;

const Exclusive = ({ children }) => {
  return <Wrapper>{children}</Wrapper>;
};

export default Exclusive;
