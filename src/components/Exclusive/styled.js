import { css } from "styled-components";
import { pixelToRem } from "../../settings";

export const base = css`
  background-color: ${(props) => props.theme.palette.primaryColor};
  padding: ${pixelToRem(4)};
  font-size: ${pixelToRem(10)};
  position: absolute;
  right: 0;
  top: ${pixelToRem(5)};
  color: white;
`;
