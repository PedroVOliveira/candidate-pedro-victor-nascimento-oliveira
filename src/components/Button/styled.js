import styled, { css } from "styled-components";

import { pixelToRem } from "../../settings";

export const base = css`
  border-radius: 5px;
  display: inline-flex;
  text-decoration: none;
  cursor: pointer;
  border-width: 1px;
  border-style: solid;
  text-transform: uppercase;
  padding: 0.9em 1.2em;
  font-weight: 400;
  white-space: nowrap;
  margin: ${pixelToRem(2)};
  align-self: center;
  border-radius: 50px;
  outline: none;
`;

export const disabled = css`
  ${(props) =>
    props.disabled &&
    `
    opacity: 0.3;
    cursor: not-allowed;
  `};
`;

export const size = css`
  ${(props) =>
    props.size === "small" &&
    `
    font-size:${pixelToRem(6)}
  `};

  ${(props) =>
    props.size === "large" &&
    `
    font-size: ${pixelToRem(12)}

  `};

  @media (min-width: 700px) {
    ${(props) =>
      props.size === "small" &&
      `
    font-size:${pixelToRem(8)}
  `};
  }
`;

export const initial = css`
  color: ${(props) => props.theme.palette.primaryTextColor};
  background-color: ${(props) => props.theme.palette.initialColor};
  border-color: ${(props) => props.theme.palette.initialColor};
  ${(props) =>
    !props.disabled &&
    `
    &:hover, &:focus {
      background-color: ${(props) => props.theme.palette.initialColor};
    }
  `}
`;

export const primary = css`
  color: white;
  background-color: ${(props) => props.theme.palette.primaryColor};
  border-color: ${(props) => props.theme.palette.primaryColor};
  ${(props) =>
    !props.disabled &&
    `
    &:hover, &:focus {
      background-color: ${props.theme.palette.primaryColor};
    }
  `}
`;
