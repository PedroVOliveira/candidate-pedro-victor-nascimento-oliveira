import React from "react";
import styled from "styled-components";
import { initial, primary, base, disabled, size } from "./styled";
const Wrapper = styled.button`
  ${base}
  ${disabled}
  ${size}
  ${(props) => props.palette === "initial" && initial};
  ${(props) => props.palette === "primary" && primary};
`;

const Button = ({
  children,
  palette = "initial",
  size = "small",
  disabled = false,
  ...rest
}) => (
  <Wrapper palette={palette} size={size} disabled={disabled} {...rest}>
    {children}
  </Wrapper>
);

export default Button;
