import React from "react";
import { ThemeProvider } from "styled-components";
import { palette } from "../settings";
import {
  GlobalStyles,
  Container,
  GridContainer,
} from "../components/GlobalStyles/GlobalStyles";
import Header from "../components/Header";
import ProductsList from "../components/ProductList";
import Notification from "../components/Notification";
import Modal from "../components/Modal";
import { ProductsProvider } from "../contexts/Products/";
import { NotificationProvider } from "../contexts/Notification";
import { ModalProvider } from "../contexts/Modal";
const App = () => {
  const theme = { palette };
  return (
    <ThemeProvider theme={theme}>
      <ModalProvider>
        <ProductsProvider>
          <NotificationProvider>
            <Header />
            <Notification />
            <Modal />
            <Container>
              <GridContainer>
                <GlobalStyles />
                <ProductsList />
              </GridContainer>
            </Container>
          </NotificationProvider>
        </ProductsProvider>
      </ModalProvider>
    </ThemeProvider>
  );
};

export default App;
