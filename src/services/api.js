export function getProducts() {
  return "https://zs5utiv3ul.execute-api.us-east-1.amazonaws.com/dev/products";
}

export function getCEP(cep) {
  return `https://zs5utiv3ul.execute-api.us-east-1.amazonaws.com/dev/freight/${cep}`;
}
